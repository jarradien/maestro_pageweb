// original examples can be found here : https://canvasjs.com/javascript-charts/

const { range } = rxjs;

window.onload = function () {
    // Faking Electrical Power Data
    // initialisation for time
    xint = [];
    xdates = [];
    minutesToMilliseconds = 60000;
    numberOfPointsPerHour = 6;
    range(24 * numberOfPointsPerHour).subscribe(r => xint.push(r));
    today = new Date();
    todayMidnight = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    // initialisation for PV electrical values
    let yEnrValue = 0;
    let yTransitingValue = 0;
    // parameters for Photovoltaic Power :
    A = 12000;                           // height of the PV Power curve
    mu = numberOfPointsPerHour * 12;    // mean (average) : numberOfPointsPerHour * (the hour of the peak)
    sigma = 4;                         // standard deviation
    let pEnR_dataPoints = [];
    // parameters for the Transiting Power
    morningHour = 8;    // 8 AM
    middayHour = 14;    // 2 PM
    eveningHour = 18;   // 6 PM
    constantPower = 0.2 * A;
    let pTransiting_dataPoints = [];

    for (i of xint) {
        pvPower = gauss(A, mu, sigma, i);
        if (pvPower > 0.05 * A) pvPowerNoise = Math.round(0.8 * Math.random()) * (0.3 * A * Math.random());
        else pvPowerNoise = 0;
        xdates.push(new Date(todayMidnight.getTime() + (60 / numberOfPointsPerHour) * i * minutesToMilliseconds));
        yEnrValue = Math.abs(pvPower - pvPowerNoise);
        pEnR_dataPoints.push({ x: xdates[xdates.length - 1], y: yEnrValue });

        morningPeak = gauss(0.9 * A, morningHour * numberOfPointsPerHour, 5, i);
        eveningPeak = gauss(1.3 * A, eveningHour * numberOfPointsPerHour, 7, i);
        middayPeak = gauss(0.7 * A, middayHour * numberOfPointsPerHour, 6, i);
        pTransitingNoise = 0.2 * A * Math.random();
        yTransitingValue = constantPower + morningPeak + middayPeak + eveningPeak + pTransitingNoise - yEnrValue;
        pTransiting_dataPoints.push({ x: xdates[xdates.length - 1], y: yTransitingValue })
    }


    // CANVAJS object
    var powerchart = new CanvasJS.Chart("powerchart", {
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        animationEnabled: true,
        title: {
            text: "Puissances électriques à Thouars"
        },
        subtitles: [{
            text: new Date().toDateString()
        }],
        axisX: {
            lineColor: "black",
            labelFontColor: "black"
        },
        axisY2: {
            gridThickness: 0.5,
            title: "Puissance (en kW)",
            suffix: " kW",
            titleFontColor: "black",
            labelFontColor: "black"
        },
        legend: {
            cursor: "pointer",
            itemmouseover: function (e) {
                e.dataSeries.lineThickness = e.chart.data[e.dataSeriesIndex].lineThickness * 2;
                e.dataSeries.markerSize = e.chart.data[e.dataSeriesIndex].markerSize + 2;
                e.chart.render();
            },
            itemmouseout: function (e) {
                e.dataSeries.lineThickness = e.chart.data[e.dataSeriesIndex].lineThickness / 2;
                e.dataSeries.markerSize = e.chart.data[e.dataSeriesIndex].markerSize - 2;
                e.chart.render();
            },
            itemclick: function (e) {
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                } else {
                    e.dataSeries.visible = true;
                }
                e.chart.render();
            }
        },
        toolTip: {
            shared: true
        },
        data: [{
            type: "spline",
            name: "P transitant",
            markerSize: 5,
            color: "blue",
            axisYType: "secondary",
            xValueFormatString: "HH:mm",
            yValueFormatString: "#,##0.0\" kW\"",
            showInLegend: true,
            dataPoints: pTransiting_dataPoints
        },
        {
            type: "spline",
            name: "P EnR",
            color: "green",
            markerSize: 5,
            axisYType: "secondary",
            xValueFormatString: "HH:mm",
            yValueFormatString: "#,##0.0\" kW\"",
            showInLegend: true,
            dataPoints: pEnR_dataPoints
            // kind of data expected by this property
            //[
            //    { x: new Date(2000, 00), y: .5 },
            //    { x: new Date(2005, 00), y: 2.4 },
            //    { x: new Date(2009, 00), y: 5.1 },
            //    { x: new Date(2010, 00), y: 7.5 },
            //    { x: new Date(2011, 00), y: 10.1 },
            //    { x: new Date(2012, 00), y: 12.6 },
            //    { x: new Date(2013, 00), y: 15.1 },
            //    { x: new Date(2014, 00), y: 18 }
            //]
        }]
    });

    powerchart.render();
};

function gauss(A, mu, sigma, x) {
    return A * Math.exp(-(x - mu) * (x - mu) / (2 * sigma * sigma));
}